package com.defaultApp.app.model

import com.google.gson.annotations.SerializedName

class DataDanaMasuk(
    @SerializedName("id")
    val id: Int? = 0,
    @SerializedName("date")
    val date: String,
    @SerializedName("time")
    val time: String,
    @SerializedName("moneyIn")
    val moneyIn: String,
    @SerializedName("from")
    val from: String,
    @SerializedName("nominal")
    val nominal: String,
    @SerializedName("keterangan")
    val keterangan: String
)