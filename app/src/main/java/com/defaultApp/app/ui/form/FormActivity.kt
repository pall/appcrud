package com.defaultApp.app.ui.form

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.PopupMenu
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModelProvider
import androidx.room.Room
import com.defaultApp.app.R
import com.defaultApp.app.dataDB.BerkahDigitalDB
import com.defaultApp.app.dataDB.DanaDB
import com.defaultApp.app.databinding.ActivityFormBinding
import com.defaultApp.app.ui.base.BaseActivity
import com.defaultApp.app.util.Utility
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.Calendar
import javax.inject.Inject
import kotlin.math.min


class FormActivity : BaseActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var binding: ActivityFormBinding
    lateinit var db: BerkahDigitalDB
    private lateinit var viewModel: FormViewModel
    var title: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_form)
        viewModel = initViewModel(FormViewModel::class.java, viewModelFactory)
        binding.viewModel = viewModel
        if (intent.getStringExtra("id") === null) {
            binding.btSimpan.text = "SIMPAN"
            title = "Tambah"
            viewModel.resetFrom()
        } else {
            binding.btSimpan.text = "UPDATE"
            title = "Update"
            viewModel.id = intent.getStringExtra("id").toString().toInt()
        }
        setUpDialog(this)
        setToolbar(title)
        initData()
        initListener()
    }

    private fun initData() {
        db = Room.databaseBuilder(
            applicationContext,
            BerkahDigitalDB::class.java, "BerkahDigitalDB"
        ).build()
        GlobalScope.launch {
            if (viewModel.id != null) {
                val data: DanaDB = db.idealDao().getItem(viewModel.id ?: 0)
                viewModel.date.set(data.date)
                viewModel.time.set(data.time)
                viewModel.moneyIn.set(data.moneyIn)
                viewModel.from.set(data.from)
                viewModel.nominal.set(data.nominal)
                viewModel.keterangan.set(data.keterangan)
                db.close()
            }
        }
    }

    private fun initListener() {
        binding.clickCalendar.setOnClickListener {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)

            val datePickerDialog = DatePickerDialog(
                this,
                { view, year, monthOfYear, dayOfMonth ->
                    binding.etDate.setText(dayOfMonth.toString() + "/" + (monthOfYear + 1) + "/" + year)
                },
                year,
                month,
                day
            )
            datePickerDialog.show()
        }

        binding.clickTime.setOnClickListener {
            val calendar = Calendar.getInstance()
            val timePickerDialog = TimePickerDialog(
                this,
                TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->
                    val selectedTime = String.format("%02d:%02d", hourOfDay, minute)
                    binding.etTime.setText(selectedTime)
                },
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                false
            )
            timePickerDialog.show()
        }

        binding.etNominal.addTextChangedListener(onCurrencyListener())

        binding.btSimpan.setOnClickListener {
            if (binding.etDate.text.toString().isEmpty() || binding.etTime.text.toString().isEmpty() || binding.etMoneyIn.text.toString().isEmpty() || binding.etFrom.text.toString().isEmpty() || binding.etNominal.text.toString().isEmpty() || binding.etKet.text.toString().isEmpty()) {
                Toast.makeText(this@FormActivity,"Masih ada data yang kosong", Toast.LENGTH_LONG).show()
            } else {
                viewModel.date.set(binding.etDate.text.toString()).toString()
                viewModel.time.set(binding.etTime.text.toString()).toString()
                viewModel.moneyIn.set(binding.etMoneyIn.text.toString()).toString()
                viewModel.from.set( binding.etFrom.text.toString().trim()).toString()
                viewModel.nominal.set(binding.etNominal.text.toString().trim()).toString()
                viewModel.keterangan.set(binding.etKet.text.toString().trim()).toString()

                GlobalScope.launch {
                    if (title == "Tambah") {
                        addData(
                            viewModel.date.get() ?: "",
                            viewModel.time.get() ?: "",
                            viewModel.moneyIn.get() ?: "",
                            viewModel.from.get() ?: "",
                            viewModel.nominal.get() ?: "",
                            viewModel.keterangan.get() ?: ""
                        )
                    } else {
                        updateData(
                            viewModel.date.get() ?: "",
                            viewModel.time.get() ?: "",
                            viewModel.moneyIn.get() ?: "",
                            viewModel.from.get() ?: "",
                            viewModel.nominal.get() ?: "",
                            viewModel.keterangan.get() ?: ""
                        )
                    }
                }

                val message = if (title == "Tambah"){
                    "Data berhasil disimpan"
                } else {
                    "Data berhasil diupdate"
                }
                Utility.showCustomDialog(this,true,message) { alertDialog ->
                    if (title == "Tambah") {
                        viewModel.resetFrom()
                    }
                    alertDialog.dismiss()
                }
            }
        }

        binding.clickDropdown.setOnClickListener {
            val popupMenu = PopupMenu(this, it)
            popupMenu.inflate(R.menu.menu_dropdown)
            popupMenu.setOnMenuItemClickListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.action_kas -> {
                        binding.etMoneyIn.setText(menuItem.title)
                        true
                    }
                    R.id.action_BCA -> {
                        binding.etMoneyIn.setText(menuItem.title)
                        true
                    }
                    R.id.action_BNI -> {
                        binding.etMoneyIn.setText(menuItem.title)
                        true
                    }
                    else -> false
                }
            }
            popupMenu.show()
        }
    }

    private fun addData(date: String, time: String, moneyIn: String, from: String, nominal: String, keterangan: String){
        val datas = DanaDB(
            date,
            time,
            moneyIn,
            from,
            nominal,
            keterangan
        )
        db.idealDao().insertAll(datas)
    }

    private fun updateData(date: String, time: String, moneyIn: String, from: String, nominal: String, keterangan: String){
        db.idealDao().updateDataById(
            viewModel.id ?: 0,
            date,
            time,
            moneyIn,
            from,
            nominal,
            keterangan
        )
    }

    private fun onCurrencyListener(): TextWatcher? {
        return object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(editable: Editable) {
                binding.etNominal.removeTextChangedListener(this)
                try {
                    val nominal = editable.toString()
                    binding.etNominal.setText(Utility.currencyRupiahFormat(nominal))
                    binding.etNominal.setSelection(binding.etNominal.text.toString().length)
                } catch (e: NumberFormatException) {
                    e.printStackTrace()
                }
                binding.etNominal.addTextChangedListener(this)
            }
        }
    }
}