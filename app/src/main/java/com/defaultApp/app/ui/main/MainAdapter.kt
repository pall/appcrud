package com.defaultApp.app.ui.main

import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.defaultApp.app.databinding.ListDanaMasukBinding
import com.defaultApp.app.model.DataDanaMasuk
import com.defaultApp.app.ui.form.FormActivity
import com.defaultApp.app.util.CustomDialog

class MainAdapter(
    val activity: MainActivity
): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var data: MutableList<DataDanaMasuk> = arrayListOf()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ItemListGenreHolder(
            ListDanaMasukBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ItemListGenreHolder -> holder.bind(data[position])
        }
    }
    inner class ItemListGenreHolder(itemView: ListDanaMasukBinding) :
        RecyclerView.ViewHolder(itemView.root) {
        val binding = itemView
        fun bind(datas: DataDanaMasuk){
            Log.d("Debug-ari","data :"+datas.keterangan)
            binding.data = datas
            binding.textViewAlias.text = datas.date+", "+datas.time
            binding.textViewPhone.text = datas.moneyIn+" "+datas.nominal
            binding.modifyAddressBtn.setOnClickListener {
                activity.startActivity(
                    Intent(activity, FormActivity::class.java)
                        .putExtra("id",datas.id.toString())
                )
            }
            binding.deleteAddressBtn.setOnClickListener {
                showConfirmAddressDeletionDialog(adapterPosition, datas.id ?: 0)
            }
        }
    }

    private fun showConfirmAddressDeletionDialog(itemPosition: Int, id: Int) {
        CustomDialog(activity).apply {
            // setTitle(activity.getString(R.string.confirm_address_deletion_header))
            setSubtitle("Yakin ingin mau menghapus data ini ?")
            setBoldPositiveLabel(true)
            setCancelable(false)
            setPositive("Ya") {
                activity.deleteItem(itemPosition,id)
                it?.dismiss()
            }
            setNegative("Tidak") {
                it?.dismiss()
            }
        }.show()
    }

}
