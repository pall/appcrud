package com.defaultApp.app.ui.form

import androidx.databinding.ObservableField
import com.defaultApp.app.cache.PreferencesHelper
import com.defaultApp.app.repository.UserRepository
import com.defaultApp.app.ui.base.BaseViewModel
import javax.inject.Inject

class FormViewModel @Inject constructor(
    val userRepository: UserRepository,
    val preferencesHelper: PreferencesHelper
) : BaseViewModel() {
    var id : Int? = null
    var date = ObservableField("")
    var time = ObservableField("")
    var moneyIn = ObservableField("")
    var from = ObservableField("")
    var nominal = ObservableField("")
    var keterangan = ObservableField("")

    fun resetFrom () {
        id = null
        date.set("")
        time.set("")
        moneyIn.set("")
        from.set("")
        nominal.set("")
        keterangan.set("")
    }
}