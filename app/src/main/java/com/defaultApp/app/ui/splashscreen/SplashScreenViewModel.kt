package com.defaultApp.app.ui.splashscreen

import com.defaultApp.app.cache.PreferencesHelper
import com.defaultApp.app.ui.base.BaseViewModel
import javax.inject.Inject

class SplashScreenViewModel @Inject constructor(
    val preferencesHelper: PreferencesHelper
) : BaseViewModel()