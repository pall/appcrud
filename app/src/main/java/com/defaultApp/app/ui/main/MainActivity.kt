package com.defaultApp.app.ui.main

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.ColumnInfo
import androidx.room.Room
import com.defaultApp.app.R
import com.defaultApp.app.dataDB.BerkahDigitalDB
import com.defaultApp.app.dataDB.DanaDB
import com.defaultApp.app.databinding.ActivityGenreBinding
import com.defaultApp.app.model.DataDanaMasuk
import com.defaultApp.app.ui.base.BaseActivity
import com.defaultApp.app.ui.form.FormActivity
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.Calendar
import javax.inject.Inject

class MainActivity : BaseActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: MainViewModel
    private lateinit var binding: ActivityGenreBinding
    lateinit var adapter: MainAdapter

    lateinit var db: BerkahDigitalDB
    var currentDate: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_genre)
        viewModel = initViewModel(MainViewModel::class.java, viewModelFactory)
        setToolbar("Dana Masuk")
        initCurrentDate()
        initAdapter()
        tesDataBase()
        initListener()
    }

    private fun initListener() {
        binding.onClickCalendar.setOnClickListener {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)

            val datePickerDialog = DatePickerDialog(
                this,
                { view, year, monthOfYear, dayOfMonth ->
                    binding.etDate.setText(dayOfMonth.toString() + "/" + (monthOfYear + 1) + "/" + year)
                    currentDate = dayOfMonth.toString() + "/" + (monthOfYear + 1) + "/" + year
                    loadData()
                },
                year,
                month,
                day
            )
            datePickerDialog.show()
        }

        binding.btnAdd.setOnClickListener {
            startActivity(
                Intent(this, FormActivity::class.java)
            )
        }
    }

    private fun initCurrentDate() {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH) + 1
        val day = calendar.get(Calendar.DAY_OF_MONTH)

        currentDate = "$day/$month/$year"
        binding.etDate.setText(currentDate)
    }

    private fun initAdapter() {
        adapter = MainAdapter(this)
        binding.rvGenre.adapter = adapter
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
        loadData()
    }

    private fun tesDataBase() {
        db = Room.databaseBuilder(
            applicationContext,
            BerkahDigitalDB::class.java, "BerkahDigitalDB"
        ).build()
//        loadData()
    }

    private fun loadData(){
        adapter.data.clear()
        GlobalScope.launch {
            val dataIsi: List<DanaDB> = db.idealDao().getCurrentDate(currentDate)
            if (dataIsi.isNotEmpty()){
                binding.viewNoData.visibility = View.GONE
                dataIsi.forEach {
                    adapter.data.add(
                        DataDanaMasuk(
                            it.id,
                            it.date,
                            it.time,
                            it.moneyIn,
                            it.from,
                            it.nominal,
                            it.keterangan
                        )
                    )
                }
            }
        }
        adapter.notifyDataSetChanged()
        if (adapter.data.isEmpty()){
            binding.viewNoData.visibility = View.VISIBLE
        } else {
            binding.viewNoData.visibility = View.GONE
        }
    }

    fun deleteItem(posisition: Int, id: Int) {
        GlobalScope.launch {
            db.idealDao().deleteItem(id)
        }
        adapter.data.removeAt(posisition)
        adapter.notifyItemRemoved(posisition)
        if (adapter.data.isEmpty()){
            binding.viewNoData.visibility = View.VISIBLE
        } else {
            binding.viewNoData.visibility = View.GONE
        }
    }
}
