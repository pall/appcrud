package com.defaultApp.app.dagger

import com.defaultApp.app.DefaultApp
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        AppModule::class
    ]
)
interface AppComponent : AndroidInjector<DefaultApp> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<DefaultApp>()
}