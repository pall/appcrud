package com.defaultApp.app.dagger

import com.defaultApp.app.ui.form.FormActivity
import com.defaultApp.app.ui.login.LoginActivity
import com.defaultApp.app.ui.main.MainActivity
import com.defaultApp.app.ui.splashscreen.SplashScreenActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
internal abstract class ActivityBuildersModule {
    @ContributesAndroidInjector
    internal abstract fun bindSplashScreenActivity(): SplashScreenActivity

    @ContributesAndroidInjector
    internal abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector
    internal abstract fun bindLoginActivity(): LoginActivity

    @ContributesAndroidInjector
    internal abstract fun bindFormActivity(): FormActivity
}