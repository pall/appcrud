package com.defaultApp.app.api.model

enum class Status {
    SUCCESS,
    ERROR,
    ACCEPTED,
    UNAUTH,
    LOADING,
    VALIDATION_ERROR
}