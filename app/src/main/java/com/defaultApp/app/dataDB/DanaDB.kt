package com.defaultApp.app.dataDB

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey


//@Entity(tableName = "income", indices = [Index(value = ["date"], unique = true)])
@Entity(tableName = "income")
data class DanaDB(
    @ColumnInfo(name = "date") val date: String,
    @ColumnInfo(name = "time") val time: String,
    @ColumnInfo(name = "moneyIn") val moneyIn: String,
    @ColumnInfo(name = "from") val from: String,
    @ColumnInfo(name = "nominal") val nominal: String,
    @ColumnInfo(name = "keterangan") val keterangan: String
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int? = null
}