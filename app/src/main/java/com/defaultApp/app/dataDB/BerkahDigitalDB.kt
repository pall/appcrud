package com.defaultApp.app.dataDB

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(
    entities = [DanaDB::class],
    version = 1,
    exportSchema = false
)
abstract class BerkahDigitalDB: RoomDatabase() {
    abstract fun idealDao(): DataDAO

    // companion object {
    //     // Singleton prevents multiple instances of database opening at the
    //     // same time.
    //     @Volatile
    //     private var INSTANCE: SearchDatabase? = null
    //
    //     fun getDatabase(context: Context): SearchDatabase {
    //         // if the INSTANCE is not null, then return it,
    //         // if it is, then create the database
    //         // return INSTANCE ?: synchronized(this) {
    //         //     val instance = Room.databaseBuilder(
    //         //         context.applicationContext,
    //         //         SearchDatabase::class.java,
    //         //         "search_database"
    //         //     ).build()
    //         //     INSTANCE = instance
    //         //     // return instance
    //         //     instance
    //         // }
    //         val tmpInstance: SearchDatabase? = INSTANCE
    //         if (tmpInstance != null){
    //             return tmpInstance
    //         }
    //         synchronized(this){
    //             val instance: SearchDatabase = Room.databaseBuilder(
    //                 context.applicationContext,
    //                 SearchDatabase::class.java,
    //                 "search_db"
    //             ).build()
    //             INSTANCE = instance
    //             return instance
    //         }
    //     }
    // }

    // companion object {
    //
    //     @Volatile
    //     private var INSTANCE: SearchDatabase? = null
    //
    //     fun getDatabase(context: Context): SearchDatabase {
    //         // if the INSTANCE is not null, then return it,
    //         // if it is, then create the database
    //         if (INSTANCE == null) {
    //             synchronized(this) {
    //                 // Pass the database to the INSTANCE
    //                 INSTANCE = buildDatabase(context)
    //             }
    //         }
    //         // Return database.
    //         return INSTANCE!!
    //     }
    //
    //     private fun buildDatabase(context: Context): SearchDatabase {
    //         return Room.databaseBuilder(
    //             context.applicationContext,
    //             SearchDatabase::class.java,
    //             "search_database"
    //         )
    //             .build()
    //     }
    // }
}