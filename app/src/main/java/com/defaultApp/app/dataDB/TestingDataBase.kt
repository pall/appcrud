package com.defaultApp.app.dataDB

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper


class TestingDataBase(context: Context?) :
    SQLiteOpenHelper(
        context,
        DATABASE_NAME,
        null,
        DATABASE_VERSION
    ) {
    private val db: SQLiteDatabase = this.writableDatabase

    // Creating Tables
    override fun onCreate(db: SQLiteDatabase) {
        val CREATE_DATA =
            "CREATE TABLE data(" +  //"id INTEGER PRIMARY KEY AUTOINCREMENT" +
                    "noTask TEXT" +
                    ",tanggal TEXT" +
                    ",jam TEXT" +
                    ",pekerjaan TEXT" +
                    ",file TEXT)"
        db.execSQL(CREATE_DATA)
    }

    // Upgrading database
    override fun onUpgrade(
        db: SQLiteDatabase,
        oldVersion: Int,
        newVersion: Int
    ) {
        //   this.ExecuteSQL("DROP TABLE IF EXISTS data");
        db.execSQL("DROP TABLE IF EXISTS data")
        onCreate(db)
    }

    fun simpanData(no_task: String?, date: String?, jam: String?, file: String?) {
        val values = ContentValues()
        values.put("no_task", no_task)
        values.put("date", date)
        values.put("jam", jam)
        values.put("file", file)
        db.insert(TABLE_NAME1, null, values)
    }

    fun lihatData(): Cursor {
        val db = this.writableDatabase
        return db.rawQuery("Select * from $TABLE_NAME1", null)
    }

    fun writeInitialData(db: TestingDataBase?) {
        //Insert new dt_item rows into table
        //db.ExecuteSQL("INSERT INTO dt_item VALUES");
        //Log.d("Insert Result","Total insertion: " + cursor.getCount() + " rows.");
    }

    fun ExecuteSQL(query: String?) {
        val db = this.writableDatabase
        db.execSQL(query)
    }

    fun SelectQuery(query: String?): Cursor {
        val db = this.writableDatabase
        return db.rawQuery(query, null)
    }

    fun hapusData() {
        val db = this.writableDatabase
        db.execSQL("delete from $TABLE_NAME1")
    }

    companion object {
        // Database Version
        private const val DATABASE_VERSION = 4

        // Database Name
        private const val DATABASE_NAME = "DataDb"
        const val TABLE_NAME1 = "data"
    }

}