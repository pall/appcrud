package com.defaultApp.app.util

interface OnLoadMoreListener {
    fun onLoadMore()
}