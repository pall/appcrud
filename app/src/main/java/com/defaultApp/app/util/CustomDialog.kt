package com.defaultApp.app.util

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import com.defaultApp.app.R

class CustomDialog(
    private val context: Context,
    title: String? = null,
    subtitle: String? = null,
    bold: Boolean = true,
    typeFace: Typeface? = null,
    cancelable: Boolean = true
) {
    private val dialog: Dialog = Dialog(context)
    private var dialogButtonOk: TextView? = null
    private var dialogButtonNo: TextView? = null
    private var titleLabel: TextView? = null
    private var subtitleLabel: TextView? = null
    private var separator: View? = null
    private var dialogFont: Typeface? = null
    private var positiveListener: CustomDialogClickListener? = null
    private var negativeListener: CustomDialogClickListener? = null
    private var negativeExist = false

    fun setPositive(okLabel: String, labelColor: Int? = null, listener: CustomDialogClickListener?) {
        positiveListener = listener
        dismiss()
        setPositiveLabel(okLabel, labelColor)
    }

    fun setNegative(koLabel: String, labelColor: Int? = null, listener: CustomDialogClickListener?) {
        if (listener != null) {
            negativeListener = listener
            dismiss()
            negativeExist = true
            setNegativeLabel(koLabel, labelColor)
        }
    }

    fun show() {
        if (!negativeExist) {
            dialogButtonNo?.isVisible = false
            separator?.isVisible = false
        }
        dialog.show()
        dialog.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
    }

    fun dismiss() = dialog.dismiss()

    fun setTitle(title: String?) {
        if (title.isNullOrEmpty()) {
            titleLabel?.isVisible = false
            subtitleLabel?.setPadding(0,16,0,0)
        } else {
            titleLabel?.isVisible = true
//            subtitleLabel?.setPaddingTop(4.dp(context).toInt())
            subtitleLabel?.setPadding(0,4,0,0)
            titleLabel?.text = title
        }
    }

    fun setSubtitle(subtitle: String?) {
        if (subtitle.isNullOrEmpty()) {
            subtitleLabel?.isVisible = false
        } else {
            subtitleLabel?.isVisible = true
            subtitleLabel?.text = subtitle
        }
    }

    fun setCancelable(cancelable: Boolean) = dialog.setCancelable(cancelable)

    private fun setPositiveLabel(positive: String, labelColor: Int?) {
        if (labelColor != null) {
            dialogButtonOk?.setTextColor(ContextCompat.getColor(context, labelColor))
        }
        dialogButtonOk?.text = positive
    }

    private fun setNegativeLabel(negative: String, labelColor: Int?) {
        if (labelColor != null) {
            dialogButtonNo?.setTextColor(ContextCompat.getColor(context, labelColor))
        }
        dialogButtonNo?.text = negative
    }

    fun setBoldPositiveLabel(bold: Boolean) {
        if (dialogFont != null) {
            if (bold) dialogButtonOk?.setTypeface(
                dialogFont,
                Typeface.BOLD
            ) else {
                dialogButtonOk?.setTypeface(dialogFont, Typeface.NORMAL)
            }
        } else {
            if (bold) {
                // dialogButtonOk?.typeface = Typeface.create("sans-serif", Typeface.BOLD)
                dialogButtonOk?.typeface = Typeface.create(
                    ResourcesCompat.getFont(
                        context,
                        R.font.roboto_regular
                    ), Typeface.BOLD
                )
            } else {
                dialogButtonOk?.typeface = Typeface.create(
                    ResourcesCompat.getFont(
                        context,
                        R.font.roboto_regular
                    ), Typeface.NORMAL
                )
            }
        }

    }

    fun setTypefaces(customFont: Typeface?) {
        if (customFont != null) {
            titleLabel?.typeface = customFont
            subtitleLabel?.typeface = customFont
            dialogButtonOk?.typeface = customFont
            dialogButtonNo?.typeface = customFont
            dialogFont = customFont
        } else {
            titleLabel?.typeface = Typeface.create(
                ResourcesCompat.getFont(
                    context,
                    R.font.roboto_regular
                ), Typeface.NORMAL
            )
            subtitleLabel?.typeface = Typeface.create(
                ResourcesCompat.getFont(
                    context,
                    R.font.roboto_regular
                ), Typeface.NORMAL
            )
            dialogButtonOk?.typeface = Typeface.create(
                ResourcesCompat.getFont(
                    context,
                    R.font.roboto_regular
                ), Typeface.NORMAL
            )
            dialogButtonNo?.typeface = Typeface.create(
                ResourcesCompat.getFont(
                    context,
                    R.font.roboto_regular
                ), Typeface.NORMAL
            )
        }
    }

    private fun initViews() {
        titleLabel = dialog.findViewById(R.id.title)
        subtitleLabel = dialog.findViewById(R.id.subtitle)
        dialogButtonOk = dialog.findViewById(R.id.dialogButtonOK)
        dialogButtonNo = dialog.findViewById(R.id.dialogButtonNO)
        separator = dialog.findViewById(R.id.separator)
    }

    private fun initEvents() {
        dialogButtonOk?.setOnClickListener {
            if (positiveListener != null) {
                positiveListener?.onClick(this)
            }
        }
        dialogButtonNo?.setOnClickListener {
            if (negativeListener != null) {
                negativeListener?.onClick(this)
            }
        }
    }

    fun interface CustomDialogClickListener {
        fun onClick(dialog: CustomDialog?)
    }

    companion object {
        private const val LOG_ERROR = "CustomDialog_ERROR"
    }

    // Constructor invocation approach
    init {
        dialog.setContentView(R.layout.custom_dialog)
        if (dialog.window != null) {
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        initViews()
        setCancelable(cancelable)

        setTitle(title)
        setSubtitle(subtitle)

        setTypefaces(typeFace)
        setBoldPositiveLabel(bold)
        initEvents()
    }

    // Builders approach: effective against Kotlin scoped functions
    constructor(ctx: Context) : this(context = ctx)
}