package com.defaultApp.app.util

import androidx.annotation.NonNull
import io.reactivex.Scheduler

interface ISchedulerProvider {

    @NonNull
    fun computation(): Scheduler

    @NonNull
    fun ui(): Scheduler
}